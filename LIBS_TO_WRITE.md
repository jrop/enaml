# Pratt Parser Generator

* Write a lib that builds an efficient, state-machine-based Lexer from a spec
  * use Rust macros/proc-macros to generate efficient code
  * support operators, ids, skippable tokens, location tracking, and custom methods?
  * no RegExp
* Write a lib that builds a Pratt Parser from a spec; use Rust macros/proc-macros to build efficient implementation

* Allow building each from spec-files: `input => serde => spec => codegen`
  * This _might_ mean that two implementations are needed for each the Lexer/Parser: one that runs at runtime, and another that gets build at compile-time?