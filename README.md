# enaml

## Inspiration

ML is really cool, but it is also a little "weird" (opinion).  Let's take inspiration from ML, but be our own thing.

## Ideas

* `;` - I like ML's `;` operator.  Instead of it being a statement separator, it is more like JavaScript's "comma operator".
* `match` - infix pattern matcher:

```
someExpression match
  | variant1 = result1
  | variant2 = result2
  | _ = genericResult
```

* `?.` as a monad `map` operator: let it be something like:
  * `getSomeOption()?.value?.subValue`, which is equivalent to:
  * `getSomeOption().map(\x=x.value).map(\x=x.subValue)`
  * the result of an expression `T?.x` is `typeof Option<T.x>`
* `!.` is similar to `?.`, but _returns_ on an "empty-type" (None/Error/Etc.):
  * `getSomeOption()!.value`.  In this case, if `getSomeOption()` is None, this will return None from the current function
* `x!`, like above, but there is no - `right` expression

* `frack` - infix destructuring operator:

```
someExpression frack { someProp1, someProp2 }
// which is equivalent to:
let someExpression = ... in
let someProp1 = someExpression.someProp1 in
let someProp2 = someExpression.someProp2 in
...
```

* `func fib(n: i64): i64 {}` or `func fib(n: i64): i64 = ...` for function syntax
* `\acc, curr = acc + curr` - lambda (`\[params]=...`)
* `let name = "World"; "Hello, {name}"` - built-in string interpolation

## Still Thinking about:

* Async
* UI: JSX?  Or Dart's approach?
* Mutability vs. Immutability...
  * Perhaps functions get their own "fork" of an object?  This seems too complicated...
  * Which is more practical?
* Adopt from Rust?: Traits + enums
* RPC: have all libraries support (built-in) a unified way for them to be called over a network.  If there is good RPC support, then writing a shell should be easy business.
* Shell: let utilities (say `ls`) be smart with knowing how they were invoked.  Is it's output a TTY?  Then invoke the text-formatter on it's data and let it display it in a human-readable format.  Is it's output another program?  Output structured data.
