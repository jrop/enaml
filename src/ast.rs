use crate::lexer::Token;

#[derive(Debug, PartialEq)]
pub enum NodeKind<'a> {
  Id(Token<'a>),
  Number(Token<'a>),
  Binary(Box<NodeKind<'a>>, Token<'a>, Box<NodeKind<'a>>),
}

impl<'a> Into<String> for &NodeKind<'a> {
  fn into(self) -> String {
    match self {
      NodeKind::Id(t) => String::from(t.text),
      NodeKind::Number(t) => String::from(t.text),
      NodeKind::Binary(left, op, right) => {
        let left: String = (&**left).into();
        let right: String = (&**right).into();
        format!("({}{}{})", left, op.text, right)
      }
    }
  }
}
