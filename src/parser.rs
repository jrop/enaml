use crate::ast::NodeKind;
use crate::lexer::{LexerState, Token, TokenKind};

use std::clone::Clone;

pub type ParsletResult<'a> = Result<NodeKind<'a>, String>;

fn get_precedence(tkn: &Token) -> usize {
  match tkn.kind {
    TokenKind::EOF | TokenKind::Unrecognized(_) => 0,
    TokenKind::Id | TokenKind::NumberLiteral => 10,
    TokenKind::Equals => 20,
    TokenKind::Plus | TokenKind::Minus => 30,
    TokenKind::Times | TokenKind::Divide => 40,
    TokenKind::TimesTimes => 50,
  }
}

fn parse_prefix<'a>(_lex: &mut LexerState<'a>, tkn: Token<'a>) -> ParsletResult<'a> {
  match tkn.kind {
    TokenKind::NumberLiteral => Ok(NodeKind::Number(tkn)),
    TokenKind::Id => Ok(NodeKind::Id(tkn)),
    _ => Err("prefix called in invalid token type".into()),
  }
}

fn parse_infix<'a>(
  lex: &mut LexerState<'a>,
  left: NodeKind<'a>,
  tkn: Token<'a>,
) -> ParsletResult<'a> {
  match tkn.kind {
    TokenKind::Plus
    | TokenKind::Minus
    | TokenKind::Times
    | TokenKind::Divide
    | TokenKind::Equals
    | TokenKind::TimesTimes => {
      let prec = get_precedence(&tkn);
      let prec = if tkn.kind != TokenKind::TimesTimes {
        prec
      } else {
        prec - 1
      };
      Ok(NodeKind::Binary(
        Box::new(left),
        tkn.clone(),
        Box::new(parse_expr(lex, prec)?),
      ))
    }
    _ => Err("infix called in invalid token type".into()),
  }
}

pub fn parse_expr<'a>(lex: &mut LexerState<'a>, rbp: usize) -> ParsletResult<'a> {
  let tkn = lex.next();
  let mut left = parse_prefix(lex, tkn)?;

  let mut tkn = lex.peek();
  while rbp < get_precedence(&tkn) {
    lex.mark_read(&tkn);
    left = parse_infix(lex, left, tkn)?;
    tkn = lex.peek();
  }
  Ok(left)
}

#[cfg(test)]
mod tests {
  #[test]
  fn test_expr() {
    use super::parse_expr;
    let tree: String = (&parse_expr(&mut "1+2*3".into(), 0).unwrap()).into();
    assert_eq!("(1+(2*3))", tree);

    let tree: String = (&parse_expr(&mut "1*2+3".into(), 0).unwrap()).into();
    assert_eq!("((1*2)+3)", tree);

    let tree: String = (&parse_expr(&mut "x=1**2**3".into(), 0).unwrap()).into();
    assert_eq!("(x=(1**(2**3)))", tree);

    let tree: String = (&parse_expr(&mut "1**2*3".into(), 0).unwrap()).into();
    assert_eq!("((1**2)*3)", tree);
  }
}
