//!
//! The Lexer for EnaML
//!

fn eat_while(s: &str, i: usize, predicate: impl Fn(char) -> bool) -> usize {
  let mut chars = s.chars().enumerate().skip(i).peekable();
  let mut last = i;
  while let Some((i, c)) = chars.peek() {
    let (i, c) = (i.clone(), c.clone());
    if predicate(c) {
      last = i + 1;
      chars.next();
    } else {
      break;
    }
  }
  last
}

#[derive(Clone, Debug, PartialEq)]
pub enum TokenKind<'a> {
  EOF,
  Unrecognized(Box<Token<'a>>),

  Id,
  NumberLiteral,

  Plus,
  Minus,
  Times,
  TimesTimes,
  Divide,
  Equals,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Token<'a> {
  pub start: usize,
  pub end: usize,
  pub text: &'a str,
  pub kind: TokenKind<'a>,
  pub skipped: Vec<Token<'a>>,
}
impl<'a> Token<'a> {
  pub fn new(
    kind: TokenKind<'a>,
    text: &'a str,
    start: usize,
    end: usize,
    skipped: Vec<Token<'a>>,
  ) -> Token<'a> {
    Token {
      text,
      start,
      end,
      kind,
      skipped,
    }
  }

  pub fn is_eof(&self) -> bool {
    self.kind == TokenKind::EOF
  }

  pub fn is_unrecognized(&self) -> bool {
    match self.kind {
      TokenKind::Unrecognized(_) => true,
      _ => false,
    }
  }
}

pub fn read_at(src: &str, pos: usize) -> Token {
  let mut chars = src.chars().enumerate().skip(pos).peekable();
  match chars.next() {
    Some((i, c)) => match c {
      '+' => Token::new(TokenKind::Plus, &src[i..i + 1], i, i + 1, vec![]),
      '-' => Token::new(TokenKind::Minus, &src[i..i + 1], i, i + 1, vec![]),
      '*' => match chars.next() {
        Some((i, '*')) => Token::new(TokenKind::TimesTimes, &src[pos..i + 1], pos, i + 1, vec![]),
        _ => Token::new(TokenKind::Times, &src[pos..i + 1], pos, i + 1, vec![]),
      },
      '/' => Token::new(TokenKind::Divide, &src[i..i + 1], i, i + 1, vec![]),
      '=' => Token::new(TokenKind::Equals, &src[i..i + 1], i, i + 1, vec![]),

      c if c.is_alphabetic() || c == '_' => {
        let i = eat_while(src, i + 1, |c| c.is_alphanumeric() || c == '_');
        Token::new(TokenKind::Id, &src[pos..i], pos, i, vec![])
      }

      n if c.is_digit(10) => {
        let i = eat_while(src, i + 1, |c| c.is_digit(10));
        // TODO: decimals
        Token::new(TokenKind::NumberLiteral, &src[pos..i], pos, i, vec![])
      }

      _ => {
        let mut next = read_at(src, i + 1);
        let next_start = next.start;
        if next.is_unrecognized() {
          next.start = pos;
          next.text = &src[pos..next.end];
          next
        } else {
          Token::new(
            TokenKind::Unrecognized(Box::new(next)),
            &src[pos..next_start],
            pos,
            next_start,
            vec![],
          )
        }
      }
    },
    None => Token::new(TokenKind::EOF, &src[pos..pos], pos, pos, vec![]),
  }
}

pub struct LexerState<'a> {
  pub source: &'a str,
  pub position: usize,
}
impl<'a> LexerState<'a> {
  pub fn new(source: &'a str) -> LexerState<'a> {
    LexerState {
      source,
      position: 0,
    }
  }
  pub fn peek(&self) -> Token<'a> {
    read_at(&self.source, self.position)
  }
  pub fn mark_read(&mut self, tkn: &Token<'a>) {
    self.position = tkn.end;
  }
  pub fn next(&mut self) -> Token<'a> {
    let t = read_at(&self.source, self.position);
    self.mark_read(&t);
    t
  }
}
impl<'a> From<&'a str> for LexerState<'a> {
  fn from(s: &'a str) -> Self {
    LexerState::new(s)
  }
}

#[cfg(test)]
mod tests {
  #[test]
  fn test_eat_while() {
    use super::eat_while;
    let i = eat_while("goo1", 0, |c| c.is_alphabetic());
    assert_eq!(i, 3);
  }

  #[test]
  fn test_read_at_operators() {
    use super::{read_at, TokenKind};
    let tkn = read_at("+-*/", 0);
    assert_eq!((tkn.start, tkn.end, tkn.kind), (0, 1, TokenKind::Plus));

    let tkn = read_at("+-*/", 1);
    assert_eq!((tkn.start, tkn.end, tkn.kind), (1, 2, TokenKind::Minus));

    let tkn = read_at("+-*/", 2);
    assert_eq!((tkn.start, tkn.end, tkn.kind), (2, 3, TokenKind::Times));

    let tkn = read_at("+-*/", 3);
    assert_eq!((tkn.start, tkn.end, tkn.kind), (3, 4, TokenKind::Divide));

    let tkn = read_at("***", 0);
    assert_eq!(
      (tkn.start, tkn.end, tkn.kind),
      (0, 2, TokenKind::TimesTimes)
    );
  }

  #[test]
  fn test_read_at_id() {
    use super::{read_at, TokenKind};
    let tkn = read_at("goo1", 0);
    assert_eq!((tkn.start, tkn.end, tkn.kind), (0, 4, TokenKind::Id));
  }
}
